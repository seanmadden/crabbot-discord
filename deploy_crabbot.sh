#!/usr/bin/env bash

crabbot_version=`./gradlew -q getVersion`
echo "crabbot version: $crabbot_version"
if [ ! -f ./build/libs/crabbot-${crabbot_version}.jar ]; then
    echo "no jar file found to deploy! exiting..."
    exit -1
fi

PID=`ssh $SSH_USER@$SSH_HOSTNAME "ps aux" | grep crabbot | awk '{print $2}'`
echo "pid is $PID"

scp ./build/libs/crabbot-${crabbot_version}.jar $SSH_USER@$SSH_HOSTNAME:~/crabbot

echo "launching crabbot v${crabbot_version}"
ssh $SSH_USER@$SSH_HOSTNAME "cd crabbot && export BOT_TOKEN=$1 && nohup java -jar crabbot-${crabbot_version}.jar > log.txt 2> errors.txt < /dev/null &"

if [ ! -z "$PID" ]; then
    echo "killing previous crabbot with pid $PID"
    ssh $SSH_USER@$SSH_HOSTNAME "kill -9 $PID"
fi
