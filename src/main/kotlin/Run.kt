package crabbot

import discord4j.core.DiscordClientBuilder
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.reaction.ReactionEmoji
import discord4j.core.event.domain.lifecycle.ReadyEvent
import discord4j.core.event.domain.message.MessageCreateEvent
import io.github.bucket4j.Bandwidth
import io.github.bucket4j.Bucket
import io.github.bucket4j.Bucket4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration
import java.util.concurrent.ConcurrentHashMap
import kotlin.random.Random


val log: Logger = LoggerFactory.getLogger("main")
val userBucketMap = ConcurrentHashMap<User, Bucket>()

fun createTokenBucket(): Bucket {
    val limit: Bandwidth = Bandwidth.simple(1, Duration.ofSeconds(30))
            .withInitialTokens(1)
    // construct the bucket
    return Bucket4j.builder().addLimit(limit).build()
}

fun getRandomNumber() = Random.nextInt(0, 100)

fun processMessage(message: Message) {

    val optionalAuthor = message.author
    if (!optionalAuthor.isPresent)
        return // I don't want to react to messages without authors.

    val author = optionalAuthor.get()

    if (!userBucketMap.containsKey(author)) {
        userBucketMap[author] = createTokenBucket()
    }

    val bucket: Bucket = userBucketMap.getValue(author)

    if (!bucket.tryConsume(1)) {
        log.debug("User ${author.username} has already had a reaction in the previous 30 seconds. Doing nothing.")
        return
    }

    val reactionsToAdd = LinkedHashSet<ReactionEmoji>()

    if (author.username.lowercase() == "malik_gynax") {
        if (getRandomNumber() < 10) {
            reactionsToAdd.add(ReactionEmoji.unicode("🌽"))
        }
    }
    if (author.username.lowercase() == "penek") {
        if (getRandomNumber() < 20) {
            reactionsToAdd.add(ReactionEmoji.unicode("🍆"))
        }
    }
    // If there's no author, I probably don't want to react to this message

    if (message.content.isNotEmpty()) {
        if (message.content.lowercase().contains("ranch")) {
            reactionsToAdd.add(ReactionEmoji.unicode("🇷"))
            reactionsToAdd.add(ReactionEmoji.unicode("🇦"))
            reactionsToAdd.add(ReactionEmoji.unicode("🇳"))
            reactionsToAdd.add(ReactionEmoji.unicode("🇨"))
            reactionsToAdd.add(ReactionEmoji.unicode("🇭"))
        }
        if (message.content.lowercase().contains("chut")) {
            reactionsToAdd.add(ReactionEmoji.unicode("🌽"))
        }

        if (message.content.lowercase().contains("crab")) {
            reactionsToAdd.add(ReactionEmoji.unicode("🦀"))
        }
        if (message.content.lowercase().contains("crabzzz")) {
            message.channel.block()
                ?.createMessage("🦀🦀🦀🦀🦀🦀🦀🦀🦀🦀🦀 !!!!CRAB ATTACK!!!! 🦀🦀🦀🦀🦀🦀🦀🦀🦀🦀")
                ?.onErrorResume { Mono.empty() }
                ?.subscribe()
        }
        if (message.content.lowercase().contains("boner")) {
            reactionsToAdd.add(ReactionEmoji.unicode("🍆"))
        }

    }

    if (getRandomNumber() < 5) {
        reactionsToAdd.add(ReactionEmoji.unicode("🦀"))
    }

    if (reactionsToAdd.isEmpty()) {
        bucket.addTokens(1)
    }
    Flux.fromIterable(reactionsToAdd)
            .flatMap { emoji -> message.addReaction(emoji)}
            .onErrorResume {
                log.error("$it")
                Mono.empty() }
            .subscribe()
}

fun main(args: Array<String>) {
    val BOT_TOKEN = System.getenv("BOT_TOKEN")
    val isDev = System.getenv("DEV")?.uppercase() == "TRUE"

    try {
        val client = DiscordClientBuilder.create(BOT_TOKEN).build()
            .withGateway { client ->
                client.eventDispatcher.on(ReadyEvent::class.java)
                    .subscribe {
                        val user = it.self.username
                        log.info("crab-bot online as user: $user")
                    }

                client.eventDispatcher.on(ReadyEvent::class.java)
                    .subscribe {
                        val user = it.self.username
                        log.info("crab-bot online as user: $user")
                    }

                client.eventDispatcher.on(MessageCreateEvent::class.java)
                    .subscribe {

                        val message = it.message
                        val guild = it.guild.block()
                        val channel = it.message.channel.block()
                        val author = it.message.authorAsMember.block()

                        log.info("[${guild?.name}>#${channel?.id}] <${author?.username}>: $message")

                        if (guild == null) {
                            return@subscribe
                        }

                        when {
                            isDev -> {
                                if (guild.name == "crabbot-test") {
                                    processMessage(message)
                                }
                            }
                            !isDev -> {
                                if (guild.name != "crabbot-test") {
                                    processMessage(message)
                                }
                            }
                        }
                    }

                return@withGateway client.onDisconnect()
            }.block()


    } catch (ex: Exception) {
        log.error(ex.toString())
    }

}
